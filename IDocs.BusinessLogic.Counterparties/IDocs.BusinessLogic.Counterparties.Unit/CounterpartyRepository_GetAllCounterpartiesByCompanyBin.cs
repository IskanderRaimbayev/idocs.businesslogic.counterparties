﻿using IDocs.BusinessLogic.Counterparties.Core.Repositories;
using IDocs.Infrastructure.Db.IDocsDb.EfContext.Public.Contexts;
using IDocs.Infrastructure.Db.IDocsDb.EfContext.Public.Entities;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace IDocs.BusinessLogic.Counterparties.Unit
{
    public class CounterpartyRepository_GetAllCounterpartiesByCompanyBin
    {
        private readonly ITestOutputHelper _output;
        public CounterpartyRepository_GetAllCounterpartiesByCompanyBin(
            ITestOutputHelper output)
        {
            _output = output;
        }

        [Fact]
        public async Task GetAllCounterpartiesByCompanyBin()
        {
            var dbContextOptions = new DbContextOptionsBuilder<IDocsDbContext>()
                .UseSqlServer(connectionString:
                    "Server=(localdb)\\MSSQLLocalDB;Integrated Security=true;Initial Catalog=idocsTestDb;")
                .Options;

            var context = new IDocsDbContext(dbContextOptions);

            await context.Database.EnsureDeletedAsync();
            await context.Database.EnsureCreatedAsync();

            var firstCompanyBin = "00000001";
            var firstCompanyToAdd = new CompanyEntity()
            {
                Bin = firstCompanyBin,
                NameRu = "IDOCS",
                NameEn = "IDOCS",
                NameKz = "IDOCS"
            };

            var secondCompanyBin = "00000002";

            var secondCompanyToAdd = new CompanyEntity()
            {
                Bin = secondCompanyBin,
                NameRu = "DEVSTREAM",
                NameEn = "DEVSTREAM",
                NameKz = "DEVSTREAM"
            };

            context.Companies.Add(firstCompanyToAdd);
            context.Companies.Add(secondCompanyToAdd);

            context.SaveChanges();

            context = new IDocsDbContext(dbContextOptions);
            var contextFacade = new IDocsDbContextFacade(context);

            var counterpartyRecord = new CounterpartyEntity
            {
                InitiatorCompanyId = firstCompanyToAdd.Id,
                CounterpartyCompanyId = secondCompanyToAdd.Id,
            };

            context.Counterparties.Add(counterpartyRecord);
            context.SaveChanges();

            var logger = new XUnitLogger<CounterpartyRepository>(_output);

            var counterpartyRepository = new CounterpartyRepository(contextFacade, logger);

            var counterparties = await counterpartyRepository
                .GetAllCounterpartiesByCompanyBin(secondCompanyBin);

            Assert.Equal(0, counterparties.Count);

            await context.Database.EnsureDeletedAsync();
        }
    }
}
