﻿using IDocs.BusinessLogic.Counterparties.Core.Repositories.Contracts;
using IDocs.BusinessLogic.Counterparties.Core.Services.Contracts;
using IDocs.Infrastructure.Db.IDocsDb.EfContext.Public.Contexts;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace IDocs.BusinessLogic.Counterparties.Core.Managers
{
    public class AddCounterpartyManager
    {
        private readonly IDocsDbContextFacade _contextFacade;

        private readonly ICounterpartyService _counterpartyService;
        private readonly ICounterpartyRepository _counterpartyRepository;
        private readonly ICompanyRepository _companyRepository;
        private readonly ICompanyMetadataService _companyMetadataService;

        private readonly ILogger<AddCounterpartyManager> _logger;

        public AddCounterpartyManager(
            IDocsDbContextFacade contextFacade, 
            ICounterpartyService counterpartyService, 
            ICounterpartyRepository counterpartyRepository,
            ICompanyRepository companyRepository,
            ICompanyMetadataService companyMetadataService, 
            ILogger<AddCounterpartyManager> logger)
        {
            _contextFacade = contextFacade;
            _counterpartyService = counterpartyService;
            _counterpartyRepository = counterpartyRepository;
            _companyRepository = companyRepository;
            _companyMetadataService = companyMetadataService;
            _logger = logger;
        }

        public async Task AddCounterparty(
            string currentCompanyBin, 
            string counterpartyCompanyBin)
        {
            var currentCompany = await _companyRepository
                .GetCompanyByBin(currentCompanyBin);

            var counterpartyCompany = await _companyRepository
                .GetCompanyByBin(counterpartyCompanyBin);

            if (counterpartyCompany == null)
            {
                var companyMetadata = await _companyMetadataService
                    .GetCompanyMetadata(counterpartyCompanyBin);
                
                var basicCompany = await _companyMetadataService
                    .GetBasicCompanyFromMetadata(companyMetadata);
                
                await _companyRepository.CreateOrUpdateCompany(basicCompany);

                var counterparty = await _counterpartyService.CreateCounterparty(currentCompany, counterpartyCompany, new string[] { });
                await _counterpartyRepository.CreateOrUpdateCounterparty(counterparty.counterpartyRecord);
            }
        }
    }
}
