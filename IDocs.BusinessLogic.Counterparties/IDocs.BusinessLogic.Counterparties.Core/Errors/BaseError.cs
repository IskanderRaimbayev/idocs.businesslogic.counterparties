﻿namespace IDocs.BusinessLogic.Counterparties.Core.Errors
{
    public abstract class BaseError
    {
        public abstract string Description { get; }
    }
}
