﻿namespace IDocs.BusinessLogic.Counterparties.Core.Errors
{
    public class CannotAddSelfAsCounterpartyError : BaseError
    {
        public override string Description => 
            "Невозможно добавить свою компанию в справочник контрагентов";
    }
}
