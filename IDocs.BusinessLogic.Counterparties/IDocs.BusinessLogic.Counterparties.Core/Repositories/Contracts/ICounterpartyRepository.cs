﻿using IDocs.BusinessLogic.Counterparties.Core.Repositories.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IDocs.BusinessLogic.Counterparties.Core.Repositories.Contracts
{
    public interface ICounterpartyRepository
    {
        Task<IReadOnlyCollection<Counterparty>> GetAllCounterpartiesByCompanyBin(
            string companyBin, CancellationToken ct = default);

        Task<Counterparty> CreateOrUpdateCounterparty(Counterparty counterparty, 
            CancellationToken ct = default);
    }
}
