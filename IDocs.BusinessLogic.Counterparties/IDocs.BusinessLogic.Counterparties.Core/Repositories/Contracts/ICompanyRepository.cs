﻿using IDocs.BusinessLogic.Counterparties.Core.Repositories.Models;
using System;
using System.Threading.Tasks;

namespace IDocs.BusinessLogic.Counterparties.Core.Repositories.Contracts
{
    public interface ICompanyRepository
    {
        Task CreateOrUpdateCompany(Company company);
        Task<Company> GetCompanyById(string companyBin);
        Task<Company> GetCompanyByBin(string companyBin);
    }
}
