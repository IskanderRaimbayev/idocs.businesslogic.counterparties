﻿namespace IDocs.BusinessLogic.Counterparties.Core.Repositories.Models
{
    public class Counterparty
    {
        public Company OwnerCompany { get; set; }
        public Company CounterpartyCompany { get; set; }
        public string [] ContactEmails { get; set; }

        public Counterparty(
            Company initiatingCompany, Company companyToAdd,
            string [] contactEmails)
        {
            OwnerCompany = initiatingCompany;
            CounterpartyCompany = companyToAdd;
            ContactEmails = contactEmails;
        }
    }
}
