﻿using System;

namespace IDocs.BusinessLogic.Counterparties.Core.Repositories.Models
{
    public class Company : IEquatable<Company>
    {
        public Guid? Id { get; set; }
        public string Name { get; set; }
        public string Bin { get; set; }

        public bool Equals(Company other)
        {
            if(other == null) return false;

            if(ReferenceEquals(this, other)) return true;
            if(Id == other.Id) return true;
            if(Bin == other.Bin) return true;

            return false;
        }

        public override bool Equals(object obj) => Equals(obj as Company);
    }
}
