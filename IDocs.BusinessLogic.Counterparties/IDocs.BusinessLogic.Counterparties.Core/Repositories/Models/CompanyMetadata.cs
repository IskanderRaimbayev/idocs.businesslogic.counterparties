﻿namespace IDocs.BusinessLogic.Counterparties.Core.Repositories.Models
{
    public class CompanyMetadata
    {
        public string Bin { get; set; }
        public string Name { get; set; }
    }
}
