﻿using IDocs.BusinessLogic.Counterparties.Core.Repositories.Contracts;
using IDocs.BusinessLogic.Counterparties.Core.Repositories.Models;
using IDocs.Infrastructure.Db.IDocsDb.EfContext.Public.Contexts;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace IDocs.BusinessLogic.Counterparties.Core.Repositories
{
    public class CompanyRepository : ICompanyRepository
    {
        private readonly IDocsDbContextFacade _dbContextFacade;
        private readonly ILogger<CompanyRepository> _logger;
        public CompanyRepository(
            IDocsDbContextFacade dbContextFacade, 
            ILogger<CompanyRepository> logger)
        {
            _dbContextFacade = dbContextFacade;
            _logger = logger;
        }

        public Task CreateOrUpdateCompany(Company company)
        {
            throw new NotImplementedException();
        }

        public Task<Company> GetCompanyByBin(string companyBin)
        {
            throw new NotImplementedException();
        }

        public Task<Company> GetCompanyById(string companyBin)
        {
            throw new NotImplementedException();
        }
    }
}
