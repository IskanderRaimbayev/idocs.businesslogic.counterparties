﻿using IDocs.BusinessLogic.Counterparties.Core.Repositories.Contracts;
using IDocs.BusinessLogic.Counterparties.Core.Repositories.Mappers;
using IDocs.BusinessLogic.Counterparties.Core.Repositories.Models;
using IDocs.Infrastructure.Db.IDocsDb.EfContext.Public.Contexts;
using IDocs.Infrastructure.Db.IDocsDb.EfContext.Public.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace IDocs.BusinessLogic.Counterparties.Core.Repositories
{
    public class CounterpartyRepository : ICounterpartyRepository
    {
        private readonly IDocsDbContextFacade _context;
        private readonly ILogger<CounterpartyRepository> _logger;
        public CounterpartyRepository(IDocsDbContextFacade context,
            ILogger<CounterpartyRepository> logger = null)
        {
            _context = context ?? 
                throw new ArgumentNullException(nameof(context));

            _logger = logger;
        }

        private CounterpartyEntity ToCounterpartyEntity(Counterparty input) =>
            new CounterpartyEntity
            {
                InitiatorCompanyId = input.OwnerCompany.Id.Value,
                CounterpartyCompanyId = input.CounterpartyCompany.Id.Value
            };

        public async Task<Counterparty> CreateOrUpdateCounterparty(Counterparty counterparty,
            CancellationToken ct = default)
        {
            var counterpartyEntity = await _context.Counterparties.SingleOrDefaultAsync(
                p => 
                    p.InitiatorCompanyId == counterparty.OwnerCompany.Id && 
                    p.CounterpartyCompanyId == counterparty.CounterpartyCompany.Id);

            if(counterpartyEntity == null)
            {
                var newCounterpartyEntity = ToCounterpartyEntity(counterparty);
                _context.Counterparties.Add(newCounterpartyEntity);
            }
            else
            {
            }

            return counterparty;
        }

        public async Task<IReadOnlyCollection<Counterparty>> GetAllCounterpartiesByCompanyBin(
            string companyBin, CancellationToken ct = default)
        {
            // Check SQL equal / lower + FirstOrDefault or + filter
            var company = await _context.Companies
                .AsNoTracking()
                .SingleOrDefaultAsync(p => companyBin.Equals(p.Bin), ct);

            if(company == null)
                return Array.Empty<Counterparty>();

            var getCounterpartiesQuery = _context.Counterparties
                .AsNoTracking()
                .Where(p => p.InitiatorCompanyId == company.Id);

            var producedSql = getCounterpartiesQuery.ToQueryString();
            _logger.LogDebug($"For {getCounterpartiesQuery} produced sql-query is '{producedSql}'");

            var counterpartyEntities = await getCounterpartiesQuery.ToArrayAsync();

            var counterparties = counterpartyEntities
                .Select(p => CounterpartyMappers.MapToCounterparty(p, company))
                .ToArray();

            return counterparties;
        }
    }
}
