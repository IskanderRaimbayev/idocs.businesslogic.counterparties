﻿using IDocs.BusinessLogic.Counterparties.Core.Repositories.Models;
using System.Threading;
using System.Threading.Tasks;

namespace IDocs.BusinessLogic.Counterparties.Core.Services.Contracts
{
    public interface ICompanyMetadataService
    {
        Task<CompanyMetadata> GetCompanyMetadata(string bin, CancellationToken ct = default);
        Task<Company> GetBasicCompanyFromMetadata(CompanyMetadata companyMetadata);
    }
}
