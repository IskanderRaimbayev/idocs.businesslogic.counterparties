﻿using IDocs.BusinessLogic.Counterparties.Core.Errors;
using IDocs.BusinessLogic.Counterparties.Core.Repositories.Models;
using System.Threading.Tasks;

namespace IDocs.BusinessLogic.Counterparties.Core.Services.Contracts
{
    public interface ICounterpartyService
    {
        Task<(Counterparty counterpartyRecord, BaseError error)> CreateCounterparty(
            Company initiatingCompany, Company companyToAdd, string [] companyContactEmails);

        Task<object[]> GetEventsForCreatedCounterparty(Counterparty counterparty);
    }
}
