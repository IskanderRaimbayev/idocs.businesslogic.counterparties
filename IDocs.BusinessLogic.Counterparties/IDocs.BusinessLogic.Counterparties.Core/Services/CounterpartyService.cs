﻿using IDocs.BusinessLogic.Counterparties.Core.Errors;
using IDocs.BusinessLogic.Counterparties.Core.Repositories.Models;
using IDocs.BusinessLogic.Counterparties.Core.Services.Contracts;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace IDocs.BusinessLogic.Counterparties.Core.Services
{
    public class CounterpartyService : ICounterpartyService
    {
        private readonly ILogger<CounterpartyService> _logger;
        public CounterpartyService(
            ILogger<CounterpartyService> logger = null)
        {
            _logger = logger;
        }

        private CannotAddSelfAsCounterpartyError CheckAgaintCannotAddSelfAsCounterpartyError(
            Company initiatingCompany, Company companyToAdd) =>
            initiatingCompany.Equals(companyToAdd) ? new CannotAddSelfAsCounterpartyError() : null;

        public Task<(Counterparty, BaseError)> GetCounterpartyToAdd(
            Company initiatingCompany, Company companyToAdd, string[] companyContactEmails)
        {
            var selfCounterpartyError = CheckAgaintCannotAddSelfAsCounterpartyError(
                initiatingCompany, companyToAdd);

            if(selfCounterpartyError != null)
            {
                _logger.LogError(selfCounterpartyError.Description);
                return Task.FromResult<(Counterparty, BaseError)>((null, selfCounterpartyError));
            }

            var counterparty = new Counterparty(initiatingCompany, companyToAdd, companyContactEmails);
            return Task.FromResult<(Counterparty, BaseError)>((counterparty, null));
        }
    }
}
